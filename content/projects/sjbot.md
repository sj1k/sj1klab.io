---
title: "sjBot"
date: 2019-04-10T16:26:18+10:00
tags:
   - "python3"
   - "irc"
link: "https://gitlab.com/sj1k/sjbot/"
---

A basic IRC bot with an async backend.

Async plugins / command system. Live command reloading, channel specific commands.

Op / moderating commands.
