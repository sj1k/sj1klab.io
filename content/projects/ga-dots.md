---
title: "ga-dots.py"
date: 2018-11-29T21:26:48+11:00
---

An experimental library for creating genetic algorithm dot games.

It comes with an example game, dots spawn and learn to avoid random obstacles to reach a goal.
