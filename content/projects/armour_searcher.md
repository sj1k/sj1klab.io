---
title: "Armour Searcher"
date: 2019-06-04T09:55:20+10:00
tags:
   - "python3"
   - "gtk"
link: "https://github.com/Sjc1000/armour_set_searcher"
---

A program for searching / creating armour sets in the game 'Monster Hunter'. Currently supports 3U, 4U and FU.
