---
title: "impd"
date: 2018-11-29T10:44:18+11:00
link: "https://gitlab.com/sj1k/impd"
tags:
   - python
   - gtk
---


A simple GTK interface to mpd. Resembles the iTunes Mini player.

MusicBrainz API support for album artwork.
