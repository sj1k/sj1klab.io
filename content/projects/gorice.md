---
title: "gorice"
date: 2019-05-21T11:01:36+10:00
draft: true
tags:
   - "go"
link: "https://gitlab.com/sj1k/gorice"
---

A simplistic dot file template system. I use go's built in text templating and a set of functions for loading variables.
