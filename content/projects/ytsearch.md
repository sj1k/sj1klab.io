---
title: "ytsearch"
tags:
   - python
   - tui
date: 2018-11-29T21:26:22+11:00
link: "https://gitlab.com/sj1k/ytsearch"
---

A command line program to search youtube and play videos with an arbitrary program.

It supports playlists, cache / downloading (via youtube-dl).
